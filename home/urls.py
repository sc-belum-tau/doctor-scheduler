from django.urls import path
from .views import *

urlpatterns = [
    path('', home, name='home'),
    path('output/', output, name='output'),
    path('upload/', upload_csv, name='upload_csv')
]
