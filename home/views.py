from django.shortcuts import render
from random import *
from math import *
from copy import *
import operator

pasienPerRS = {}
dokter = []
populasi = []
crossoverPattern = {}
jumlahGA = 20
jumlahRS = 0
hasilAkhir ={}
# jumlahPasienTotal = 0
# Create your views here.

# Sample List RS dan jumlah Pasien per RS

# def main():
#     #Mengenerate sample individu dan populasi
#     populasi = []
#     individu = {}
#     for j in range(4) :
#         for i in dokter :
#             individu[i] = choice(list(pasienPerRS.keys()))
#         populasi.append(individu)
#         individu = {}

# Generate pattern for crossover


def home(request):
    return render(request, 'home.html')


def upload_csv(request):
    data = []
    population = []
    global jumlahPasienTotal
    if "GET" == request.method:
        return render(request, "home.html", {})
    print(request.FILES['csv_file'])
    # try:
    csv_file = request.FILES['csv_file']
    print(csv_file.name.endswith('.csv'))
    if not csv_file.name.endswith('.csv'):
        # messages.error(request, 'File is not CSV type')
        return render(request, "home.html", {})
    file_data = csv_file.read().decode("utf-8")
    print(file_data)
    lines = file_data.split("\n")
    # loop over the lines and save them in model. If error , store as string and then display

    listjam = []
    for line in lines[1:]:
        row = line.split(",")
        if row[0] == '' :
            dokter.append(row[3])
            continue
        elif row[0] not in listjam:
            listjam.append(row[0])
        dokter.append(row[3])
    
    global pasienPerRS
    global jumlahRS
    global jumlahTerlayaniMax
    for jam in listjam:
        for line in lines[1:]:
            row = line.split(",")
            if row[0] == jam:
                pasienPerRS[row[1]] = int(row[2])
        # except Exception as e:
        #     # logging.getLogger("error_logger").error(
        #     #     "Unable to upload file. "+repr(e))
        #     # messages.error(request, "Unable to upload file. "+repr(e))
        #     # population = population_generator(data)
        print(pasienPerRS)
        jumlahRS = len(pasienPerRS.keys())
        jumlahTerlayaniMax = jumlahRS * 3
        jumlahPasienTotal = sum(pasienPerRS.values())
        hasilAkhir[jam]=population_generator()
        pasienPerRS = {}
    
    print('Akhir : ', hasilAkhir)
    hasil = {'hasilAkhir':hasilAkhir}
    # return render(request, "output_jadwal.html", {'data': population})
    return render(request, 'output_jadwal.html', hasil)


def output(request):
    return render(request, 'output_jadwal.html')


# Genetic Algorithm here

# Mengenerate sample individu dan populasi
def population_generator():

    individu = {}
    for j in range(4):
        for i in dokter:
            individu[i] = choice(list(pasienPerRS.keys()))
        populasi.append(individu)
        individu = {}
    # print(populasi[0])
    # print(fitness_function(populasi[0]))

    for i in populasi[0]:
        crossoverPattern[i] = randint(0, 1)

    hasil = mainGA(populasi)
    print(hasil)
    print(fitness_function(populasi[0]), fitness_function(hasil))
    return hasil


def testing(popa, popb):
    # Mengenerate sample individu dan populasi
    populasi = []
    individu = {}
    for j in range(4):
        for i in dokter:
            individu[i] = choice(list(pasienPerRS.keys()))
        populasi.append(individu)
        individu = {}

    x = 0
    y = 0
    for i in popa:
        x += fitness_function(i)
    for ii in popb:
        y += fitness_function(ii)
    print(x, y)

# Main yuk yukyukykukyuk GA


def mainGA(population):
    generasiBaru = population
    for i in range(jumlahGA):
        generasiBaru = geneticAlgortihm(generasiBaru)

    champion = findBest(generasiBaru)
    return(champion)


def copyPopulation(population):
    cpy = []
    for dic in population:
        d2 = deepcopy(dic)
        cpy.append(d2)
    return cpy

# Genetic Algorithm here


def geneticAlgortihm(population):
    newGeneration = []
    newPopulation = copyPopulation(population)
    jumlahIterasi = len(newPopulation)
    for i in range(0, jumlahIterasi):
        selectionResult = selection(newPopulation)
        crossoverResult = crossover(selectionResult)
        mutationResult = mutation(crossoverResult)
        champion = findBest(mutationResult)
        newGeneration.append(champion)

    return newGeneration


def findBest(population):
    x = 0
    best = {}
    for individu in population:
        fitness = fitness_function(individu)
        if fitness > x:
            x = fitness
            best = individu
    return best


def selection(population):
    tmp = copyPopulation(population)
    selectionResult = []
    for i in range(len(tmp)//2):
        randidx1 = randint(0, len(tmp)-1)
        ind1 = tmp[randidx1]
        del tmp[randidx1]
        randidx2 = randint(0, len(tmp)-1)
        ind2 = tmp[randidx2]
        del tmp[randidx2]

        fitness_ind1 = fitness_function(ind1)
        fitness_ind2 = fitness_function(ind2)

        if fitness_ind1 > fitness_ind2:
            selectionResult.append(ind1)
        elif fitness_ind1 < fitness_ind2:
            selectionResult.append(ind2)
        else:
            rand = sample([ind1, ind2], 1)
            selectionResult.append(rand[0])
    return selectionResult

# crossover function


def crossover(population):
    tmp = copyPopulation(population)
    crossoverResult = []

    for i in range(len(tmp)//2):
        randidx1 = randint(0, len(tmp)-1)
        ind1 = deepcopy(tmp[randidx1])
        del tmp[randidx1]
        randidx2 = randint(0, len(tmp)-1)
        ind2 = deepcopy(tmp[randidx2])
        del tmp[randidx2]

        for j in crossoverPattern:
            if (crossoverPattern[j] == 1):
                ind1[j], ind2[j] = ind2[j], ind1[j]

        crossoverResult.append(ind1)
        crossoverResult.append(ind2)

    return crossoverResult


# mutation function : swap
def mutation(population):
    mutatedPopulation = []
    mutationResult = {}
    for individu in population:
        tmp = copy(list(individu.keys()))
        dokterSwap1 = choice(tmp)
        tmp.remove(dokterSwap1)
        dokterSwap2 = choice(tmp)

        individucopy = deepcopy(individu)
        individucopy[dokterSwap2], individucopy[dokterSwap1] = individucopy[dokterSwap1], individucopy[dokterSwap2]

        fitness_ind1 = fitness_function(individu)
        fitness_ind2 = fitness_function(individucopy)

        if fitness_ind1 > fitness_ind2:
            mutationResult = ind1
        elif fitness_ind1 < fitness_ind2:
            mutationResult = ind2
        else:
            mutationResult = choice([individu, individucopy])
        mutatedPopulation.append(mutationResult)

    return mutatedPopulation
# Fitness function total


def fitness_function(individu):
    global jumlahPasienTotal
    global jumlahTerlayaniMax
    # print('tidakterlayani :',fitness_tidak_terlayani(individu),end=';')
    # print('pasienmax :',fitness_pasien_max(individu),end=';')
    # print('total :',normalize(jumlahPasienTotal, (fitness_tidak_terlayani(individu))) + (0.25 * normalize(jumlahTerlayaniMax, fitness_pasien_max(individu))))
    return normalize(jumlahPasienTotal, (fitness_tidak_terlayani(individu))) + (0.25 * normalize(jumlahTerlayaniMax, fitness_pasien_max(individu)))


# Fitness 1 (Banyaknya jumlah pasien yang tidak terlayani)
def fitness_tidak_terlayani(individu):

    tmpJumlahTotaltakTerlayani = deepcopy(pasienPerRS)

    for i in individu.keys():
        rs = individu[i]
        if (tmpJumlahTotaltakTerlayani[rs] - 3) <= 0:
            tmpJumlahTotaltakTerlayani[rs] = 0
        else:
            tmpJumlahTotaltakTerlayani[rs] = tmpJumlahTotaltakTerlayani[rs] - 3
    
    totalTidakTerlayani = 0
    # print(tmpJumlahTotaltakTerlayani)
    for i in tmpJumlahTotaltakTerlayani.keys():
        totalTidakTerlayani += tmpJumlahTotaltakTerlayani[i]
        # print(totalTidakTerlayani)

    fitnessTidakTerlayani = totalTidakTerlayani
    totalTidakTerlayani = 0
    return fitnessTidakTerlayani

# Fitness 2 : Pasien maximum per sesi per rumah sakit


def fitness_pasien_max(individu):

    maxPasienPerDokterPerRS = {}
    tmpPasienPerRS = deepcopy(pasienPerRS)

    for i in individu.keys():
        rs = individu[i]

        if rs in maxPasienPerDokterPerRS.keys():
            continue

        counter = 0
        for ii in individu.keys():
            if individu[ii] == rs:
                counter += 1
            continue
        maxPasienPerDokter = 0
        if (tmpPasienPerRS[rs] >= (counter*3)):
            maxPasienPerDokter = 3
        else:
            maxPasienPerDokter = ceil((tmpPasienPerRS[rs] / counter))

        maxPasienPerDokterPerRS[rs] = maxPasienPerDokter

    totalPasienMax = 0

    for i in maxPasienPerDokterPerRS.keys():
        totalPasienMax += maxPasienPerDokterPerRS[i]

    fitnessPasienMax = totalPasienMax
    return fitnessPasienMax


def normalize(maks, val):
    return (maks-val)/maks
